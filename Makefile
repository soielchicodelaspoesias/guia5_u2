prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = avl.cpp grafo.cpp
OBJ = avl.o grafo.o
APP = avl

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
