#include <fstream>
#include <iostream>
#include <string>
#include "Grafo.h"
using namespace std;

Grafo::Grafo(){}
// se crea la variable fp con la cual se escribe en el txt
ofstream fp;

// funcion que genera el txt
void Grafo::GenerarGrafo(NODO *ArbolInt) {
  // se genera el txt
  fp.open("grafo.txt");
  fp << "digraph G {" << endl;
  fp << "node [style=filled fillcolor=yellow];" << endl;

  fp << "nullraiz [shape=point];" << endl;
  fp << "nullraiz->" << "\"" << ArbolInt->info << "\"" << " ";
  fp << "[label=" << ArbolInt->FE << "];" << endl;
  // se llama a la funcion que recorre
  PreOrden(ArbolInt);

  fp << "}";
  // se ciera el txt
  fp.close();
  // comando que genera la imagen a partir del txt
  system("dot -Tpng -ografo.png grafo.txt");
  // comando que abre la imagen
  system("eog grafo.png &");
}


void Grafo::PreOrden(NODO *a) {
  // el nodo tiene que ser distinto de NULL
  if (a != NULL) {
    // se escribe el nodo y la rama izquierda
    if (a->izq != NULL) {
      fp << "\"" << a->info << "\"" << "->" << "\"" <<a->izq->info << "\"" << "[label=" <<a->izq->FE  << "];" << endl;
    } else{
      fp <<  "\"" << a->info << "i\"" << " [shape=point];" << endl;
      fp << "\"" <<a->info << "\"" << "->" << "\"" << a->info << "i\"" << ";" << endl;
    }
    // se escribe la rama derecha
    if (a->der != NULL) {
      fp << "\"" <<a->info << "\"" << "->" << "\"" << a->der->info << "\"" << "[label=" <<a->der->FE << "];" << endl;;
    } else{
      fp <<  "\"" << a->info << "d\""  << " [shape=point];" << endl;
      fp << "\"" << a->info << "\"" << "->" << "\"" << a->info << "d\"" << ";" << endl;
    }
    // se recorre el arbol por recursividas, primero izquierda despues derecha
    PreOrden(a->izq);
    PreOrden(a->der);
  }
}
