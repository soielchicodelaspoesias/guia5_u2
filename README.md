# guia5_u2
*Arboles Balanceados AVL*

***Pre-requisitos***

- C++
- Make
- Graphviz
- Ubuntu

***Instalación***

- _Instalar Make._

    abrir terminal y escriba el siguiente codigo:
    #sudo apt install make

- _Instalar Graphviz._

    abrir terminal , entre como superusuario y escriba el siguiente codigo:
    #apt-get install graphviz 



***Problematica***

Se debe reescribir el programa de c a c++ para que cargue los datos de un txt con ids de proteinas, ademas el usuario puede inserta, eliminar, modificar un elemento (eliminar valor viejo, e insertar uno nuevo) y ademas tener una opcion que genere el grafo

***Ejecutando***

- Para ejecutar el programa debe tener todos los archivos de este repositorio en una misma carpeta, depues en la terminal usar el comando: **_make_**, esperar que compile y ejecutar el programa con el siguiente comando: **_./avl_**

Al ejecutar el programa se le pedira que elija que archivo txt, si ingresa 1, podra cargar el archivo "pdbs_completo.txt" advertencia si usted cargar este archivo es posible que el programa se pegue y tenga que cerrar la terminal ya que este archivo tiene demasiados ids. Se ingresa un 0 podra cargar el archivo que solo contiene 100 ids, y el programa podra generar el grafo con este txt.

Despues con un menu de 7 opciones, la primera sera "Cargar datos" en esta opcion se genera el grafo segun el txt escojido, posteriormente se mostrara el arbol. La segunda opcion es insertar datos en esta se le pedira el nodo que desea insertar, despues se mostrara el arbol con su nodo insertado. La tercera opcion es modificar un elemento en este se le pedira el nodo a modificar, si el nodo no esta, se dira en pantalla y retornara al menu, si el nodo esta se elimina el nodo antiguo y se le pide un nodo nuevo al usuario y se inserta. La cuarta opcion sera eliminar un nodo en esta le pedira el nodo a eliminar, y se borrara este, si el nodo que desea elimnar no exite se le mostrara en pantalla y retornara al menu. La quinta opcion es buscar por si a simple vista no se logra encontrar algun nodo, la sexta opcion es "grafo" en esta opcion se mostrara el arbol. Finalmente la septima opcion sera salir y se terminara el programa.

***Construido con C++***

***Librerias***

- Stdlib.h
- Fstream
- Stdio.h
- Iostream

***Version 0.1***

***Autor***

Luis Rebolledo



