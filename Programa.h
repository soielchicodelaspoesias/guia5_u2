#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <iostream>
using namespace std;

// se define el struct nodo
typedef struct _NODO {
  // crea el lado derecho y el lado izquierdo su FE y su informacion
  struct _NODO *izq;
  struct _NODO *der;
  string info;
  int FE;

  // funciones
  void InsercionBalanceado(_NODO *&nodocabeza, int *BO, string infor);
  void Busqueda(_NODO *&nodo, string infor, int &resultado);
  void Restructura1(_NODO *nodocabeza, int *BO);
  void Restructura2(_NODO *nodocabeza, int *BO);
  void Borra(_NODO **aux1, _NODO **otro1, int *BO);
  void EliminacionBalanceado(_NODO *nodocabeza, int *BO, string infor);

} NODO;
