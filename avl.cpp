#include <stdlib.h>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include "Grafo.h"

using namespace std;

#define TRUE 1
#define FALSE 0

// FUNCIONES

// insertar nodos
void InsercionBalanceado(NODO *&nodocabeza, int *BO, string infor) {
  // VARIABLES AUXILIARES
  NODO *nodo = NULL;
  NODO *nodo1 = NULL;
  NODO *nodo2 = NULL;

  // se iguala al auxiliar para trabajar con el auxiliar
  nodo = nodocabeza;

  // el nodo tiene que ser distinto de NULL
  if (nodo != NULL) {

    // se compara los string, si el compare retorna un -1 infor es menor que nodo->info
    if (infor.compare(nodo->info) == -1) {
      // se llama por recursividad con el nodo izquierdo
      InsercionBalanceado((nodo->izq), BO, infor);
      // si el BO es true se ven 3 casos para el valor de nodo->FE
      if(*BO == TRUE) {
        switch (nodo->FE) {
          // nodo->FE = 1
          case 1:
            // se cambia el valor del nodo->FE por un 0
            nodo->FE = 0;
            // el BO se cambia por un FALSE
            *BO = FALSE;
            break;
          // nodo->FE = 0
          case 0:
            // se cambia el valor del nodo->FE por un -1
            nodo->FE = -1;
            break;
          // nodo->FE = -1
          case -1:
            // Se nececita reestructurar el arbol
            nodo1 = nodo->izq;

            // si el Fe es menor o igual a 0 la rotacion es II
            if (nodo1->FE <= 0) {
              nodo->izq = nodo1->der;
              nodo1->der = nodo;
              nodo->FE = 0;
              nodo = nodo1;

              // sino la rotacion es ID
            } else {
              // se trabaja con una variable aux
              nodo2 = nodo1->der;
              nodo->izq = nodo2->der;
              nodo2->der = nodo;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;
              // se trabaja segun el valor de nodo2->FE
              if (nodo2->FE == -1)
                nodo->FE = 1;
              else
                nodo->FE =0;

              if (nodo2->FE == 1)
                nodo1->FE = -1;
              else
                nodo1->FE = 0;
              // al final se iguala para guardar los cambios
              nodo = nodo2;
            }
            nodo->FE = 0;
            *BO = FALSE;
            break;
        }
      }

    } else {
      // se compara los string, si el compare retorna un 1 infor es mayor que nodo->info
      if (infor.compare(nodo->info) == 1) {
        // se recorre el arbol por el lado derecho
        InsercionBalanceado((nodo->der), BO, infor);
          // si el BO es true se ven 3 casos para el valor de nodo->FE
        if (*BO == TRUE) {
          switch (nodo->FE) {
            // nodo->FE = -1
            case -1:
              // el FE se iguala a 0
              nodo->FE = 0;
              // el BO se vuelve FALSE
              *BO = FALSE;
              break;
            // nodo->FE = 0
            case 0:
              // nodo->FE se le da el valor de 1
              nodo->FE = 1;
              break;
            // nodo->FE = 1
            case 1:
              // se necesita reestructurar el arbol
              // se utiliza la variable temporal
              nodo1 = nodo->der;
              // si nodo->FE es mayor 0 igual a 0 la rotacion es DD
              if (nodo1->FE >= 0) {
                nodo->der = nodo1->izq;
                nodo1->izq = nodo;
                nodo->FE = 0;
                nodo = nodo1;
                // sino la rotacion es DI
              } else {
                // se utiliza otra variable temporal
                nodo2 = nodo1->izq;
                nodo->der = nodo2->izq;
                nodo2->izq = nodo;
                nodo1->izq = nodo2->der;
                nodo2->der = nodo1;

                // si nodo2->FE = 1
                if (nodo2->FE == 1)
                  // el FE del nodo original es -1
                  nodo->FE = -1;
                // sino se igula a 0
                else
                  nodo->FE = 0;
                // nodo2 = -1
                if (nodo2->FE == -1)
                  // el nodo1 se cambia por uno 1
                  nodo1->FE = 1;
                // sino
                else
                  // el FE de nodo se iguala a 0
                  nodo1->FE = 0;
                // se igualan los nodos para guardar los cambios
                nodo = nodo2;
              }
              nodo->FE = 0;
              *BO = FALSE;
              break;
          }
        }
      // si el compare no es igual a 1 o -1 significa que los ids son iguales
      } else {
        printf("El nodo ya se encuentra en el árbol\n");
      }
    }
  // si el nodo es == NULL se ingresa el primer nodo
  } else {
    nodo = new NODO();
    nodo->izq = NULL;
    nodo->der = NULL;
    nodo->info = infor;
    nodo->FE = 0;
    *BO = TRUE;
  }
  // se igualan los nodos para guardar los cambios
  nodocabeza = nodo;
}

// funcion que reestructura
void Restructura1(NODO **nodocabeza, int *BO) {
  // se cran las varialbes temporales
  NODO *nodo, *nodo1, *nodo2;
  // se iguala el aux al cabeza para trabajar con el aux
  nodo = *nodocabeza;
  // si el BO es TRUE
  if (*BO == TRUE) {
    // se trabajan con varios casos dependiendo del valor de nodo->FE
    switch (nodo->FE) {
      // si nodo->FE = -1
      case -1:
        // se le da un valor de -1
        nodo->FE = 0;
        break;
      // nodo->FE = 0
      case 0:
        // se le da el valor de 1 y BO se hace FALSE
        nodo->FE = 1;
        *BO = FALSE;
        break;
    // nodo->FE = 1
    case 1:
      // se reestructura el Arbol
      // se iguala al aux para trabajar con este
      nodo1 = nodo->der;
      // si el nodo->de es mayor o igual a 0 la rotacion es DD
      if (nodo1->FE >= 0) {
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;
        // nodo->FE es igual 0
        switch (nodo1->FE) {
          case 0:
            // se cambian los FE de los nodos a -1 y BO se hace FALSE
            nodo->FE = 1;
            nodo1->FE = -1;
            *BO = FALSE;
            break;
          // nodo->FE = 1
          case 1:
            // SE cambian los FE de los nodos a 0 y Bo se hace false
            nodo->FE = 0;
            nodo1->FE = 0;
            *BO = FALSE;
            break;
        }
        // se igualan para guardar los cambios
        nodo = nodo1;
      } else {
        // sino la rotacion se hace DI
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;
        // si nodo2->FE es = 1
        if (nodo2->FE == 1)
          // el nodo->FE se hace -1
          nodo->FE = -1;
        // sino se hace = 0
        else
          nodo->FE = 0;
        // si nodo2->Fe = -1
        if (nodo2->FE == -1)
          // el Fe del nodo1 se hace 1
          nodo1->FE = 1;
        // sino se hace 0
        else
          nodo1->FE = 0;

        nodo = nodo2;
        nodo2->FE = 0;
      }
      break;
    }
  }
  // se iguala para guardar los cambios
  *nodocabeza=nodo;
}

// funcion que reestructura v2
void Restructura2(NODO **nodocabeza, int *BO) {
  NODO *nodo, *nodo1, *nodo2;
  //se iguala al auxiliar para trabajar con este
  nodo = *nodocabeza;
  // si el BO es TRue
  if (*BO == TRUE) {
    // se trabaja segun el valor de nodo->FE
    switch (nodo->FE) {
      // nodo->FE = 1
      case 1:
        // FE se hace 0
        nodo->FE = 0;
        break;
      // nodo->FE = 0
      case 0:
        // FE se hace -1
        nodo->FE = -1;
        // y el BO se hace FALSE
        *BO = FALSE;
        break;
      // nodo->FE = -1
      case -1:
        // Se necesita reestructurar el arbol
        // se trabaja con un aux
        nodo1 = nodo->izq;
        // si el nodo1->FE es menor o igual a o la rotacion es II
        if (nodo1->FE<=0) {
          nodo->izq = nodo1->der;
          nodo1->der = nodo;
          // se trabaja segun el valor de nodo1->FE
          switch (nodo1->FE) {
            // nodo1->FE = 0
            case 0:
              nodo->FE = -1;
              nodo1->FE = 1;
              *BO = FALSE;
              break;
            // nodo1->FE = -1
            case -1:
              nodo->FE = 0;
              nodo1->FE = 0;
              *BO = FALSE;
              break;
          }
          // se guardan los cambios
          nodo = nodo1;
        } else {
          // sino la rotacion tiene que ser ID
          // se trabajan con las variables aux
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;
          // se trabaja dependiendo del valor de nodo2->FE
          if (nodo2->FE == -1)
            nodo->FE = 1;
          else
            nodo->FE = 0;

          if (nodo2->FE == 1)
            nodo1->FE = -1;
          else
            nodo1->FE = 0;
          // se guardan los cambios
          nodo = nodo2;
          nodo2->FE = 0;
        }
        break;
    }
  }
  // se guardan los cambios
  *nodocabeza = nodo;
}
// borra
void Borra(NODO **aux1, NODO **otro1, int *BO) {
  // se crean las variables aux
  NODO *aux, *otro;
  // se iguala al los nodos para trabajar con las aux
  aux = *aux1;
  otro= *otro1;
  // si el lado derecho es distinto de nulo
  if (aux->der != NULL) {
    // se llama por recursividad borra con aux->derecho y Restructura2
    Borra(&(aux->der), &otro,BO);
    Restructura2(&aux,BO);
  } else {
    //sino se iguala al lado izq
    otro->info = aux->info;
    aux = aux->izq;
    *BO = TRUE;
  }
  // se guardan los cambios
  *aux1=aux;
  *otro1=otro;
}

void EliminacionBalanceado(NODO **nodocabeza, int *BO, string infor) {
  NODO *nodo, *otro;
  // se iguala al nodo temporal para trabajar con el
  nodo = *nodocabeza;
  // el nodo tiene que ser distinto de NULL
  if (nodo != NULL) {
    // si el compare devuelve un - 1 infor es menor a nodo->info
    if (infor.compare(nodo->info) == -1) {
      // se recorre el arbol por el lado derecho
      EliminacionBalanceado(&(nodo->izq),BO,infor);
      // se llama a reestructura
      Restructura1(&nodo,BO);
    } else {
      // si el compare es igual a 1 es por que infor es mayor a nodo->info
      if (infor.compare(nodo->info) == 1) {
        // se llama a eliminacion por el lado derecho
        EliminacionBalanceado(&(nodo->der),BO,infor);
        // se reestructura
        Restructura2(&nodo,BO);
      } else {
        // sino se igualan a un aux para trabajar con el aux
        otro = nodo;
        // si no tiene lado derecho
        if (otro->der == NULL) {
          // el nodo es igual al aux->izq
          nodo = otro->izq;
          *BO = TRUE;
        } else {
          // si el aux->izq es igual a NULL
          if (otro->izq==NULL) {
            // el nodo es igual al aux->der
            nodo=otro->der;
            *BO=TRUE;
          } else {
            // sino se llama a borrar con el lado izq y se reestructura
            Borra(&(otro->izq), &otro,BO);
            Restructura1(&nodo,BO);
            // se elimina la memoria que ocupa el nodo
            otro = NULL;
          }
        }
      }
    }
  } else {
    printf("El nodo NO se encuentra en el árbol\n");
  }
  // se igualan para guardar los cambios
  *nodocabeza=nodo;
}
// funcion que busca el nodo
void Busqueda(NODO *nodo, string infor, int &resultado) {
  // el nodo es distinto a NULL
  if (nodo != NULL) {
    // Se recorre el arbol con recursividad por el lado izquierdo
    if (infor.compare(nodo->info) < 0) {
      Busqueda(nodo->izq,infor, resultado);
    } else {
      // se recorre el arbol con recursividad por el lado derecho
      if (infor.compare(nodo->info) > 1) {
        Busqueda(nodo->der,infor, resultado);
      } else {
        cout << "El nodo Si esta en el arbol" << endl;
        // sino es por que lo encontro y a resultado se le suma uno
        resultado++;
      }
    }
  } else {
    // sino el nodo no se necontro
    cout << "El nodo NO se encuentra en el árbol"<< endl;
  }
}

int main() {
  // se crean las variables
  int resultado;
  Grafo g = Grafo();
  string elemento;
  int opcion;
  int eleccion;
  string txt;
  // se le pregunta al usuario que archivo desea cargar
  cout << "Que archivo txt desea cargar" << endl;
  cout << "pdbs_completo{1} - pdbs_100{0}: ";
  cin >> eleccion;
  // si carga el archivo completo es posible que el programa no le corra
  if(eleccion==1)
    txt = "pdbs_completo.txt";
  // si corre el programa con el txt que contiene solo 100 id el programa funciona
  if(eleccion==0)
    txt = "pdbs_100.txt";
  ifstream archivo(txt);
  string linea;
  NODO *raiz = NULL;
  int inicio;
  string modifica;

do{
  cout << "\n--------------------\n";
  cout << "1) cargar datos" << endl;
  cout << "2) Insertar" << endl;
  cout << "3) Modificar(elimina el valor viejo e ingresa uno nuevo)" << endl;
  cout << "4) Eliminacion" <<endl;
  cout << "5) Buscar" <<endl;
  cout << "6) Grafo" << endl;
  cout << "7) Salir" <<  endl;;

  cout << "Opción: ";
  cin >> opcion;

    switch(opcion) {
      case 1:
        // se recorre el archivo que contiene los pdbs, linea es taca linea del txt
        while(getline(archivo, linea)){
          // se llama a la funcion insertar enviendole cada linea
          inicio=FALSE;
          InsercionBalanceado(raiz, &inicio, linea);
        }
        // se genera el grafo
        g.GenerarGrafo(raiz);
        break;

      case 2:
        // se le pide un elemento al usuario
        cout << "Ingresar elemento: ";
        cin >> elemento;
        inicio=FALSE;
        // se llama a la funcion insertar y se le envia el elemnto
        InsercionBalanceado(raiz, &inicio, elemento);
        // se genera elgrafo
        g.GenerarGrafo(raiz);
        break;

      case 3:
        // se le pide al usuario el nodo a buscar
        cout << "Buscar elemento: ";
        cin >> elemento;
        // se busca y si esta en el arbol resultado = 1
        Busqueda(raiz, elemento, resultado);
        if(resultado == 1){
          // se le cambia el valor a resultado
          resultado = 0;
          inicio=FALSE;
          // se elimina el valor encontrado
          EliminacionBalanceado(&raiz, &inicio, elemento);
          // se le pide el nuevo elemento
          cout << "Ingresar nuevo elemento: ";
          cin >> modifica;
          inicio = FALSE;
          // se llama a insertar y se ele envia el modifica
          InsercionBalanceado(raiz, &inicio, modifica);
          // se genera el grafo
          g.GenerarGrafo(raiz);
        }
        break;

      case 4:
        // le pide el nodo a Eliminar
        cout << "Eliminar elemento: ";
        cin >> elemento;
        inicio=FALSE;
        //se llama a eleminar y se genera el grafo
        EliminacionBalanceado(&raiz, &inicio, elemento);
        g.GenerarGrafo(raiz);
        break;

      case 5:
          // se le pide al usuario el nodo a buscar
          cout << "Buscar elemento: ";
          cin >> elemento;
          Busqueda(raiz, elemento, resultado);
          break;
      case 6:
        // se genera el grafo
        g.GenerarGrafo(raiz);
        break;

      }
    // si la opcion es distinta de 7 se sigue repitinedo el menu
  }while(opcion != 7);
  return 0;
}
